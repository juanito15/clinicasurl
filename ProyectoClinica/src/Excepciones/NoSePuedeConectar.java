/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Excepciones;

/**
 * Clase para manejo de excepciones en caso de no poder conectarse a la BD
 * @author juanm
 */
public class NoSePuedeConectar extends Exception{
    /**
     * Constructor con mensaje de error
     * @param msj mensaje a enviar
     */
    public NoSePuedeConectar(String msj){
        super(msj);
    }
}
