/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Excepciones;

/**
 * Clase para manejo de excepciones
 * @author juanm
 */
public class UsuarioNoExiste extends Exception{
    /**
     * Constructor con mensaje personalizado
     * @param msj mensaje
     */
    public UsuarioNoExiste(String msj){
        super(msj);
    }
}
