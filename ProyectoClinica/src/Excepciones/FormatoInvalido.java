/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Excepciones;

/**
 * Clase para manejo de excepciones
 * @author juanm
 */
public class FormatoInvalido extends Exception {
    /**
     * Crea una nueva excepción con mensaje personalizado
     * @param msj mensaje
     */
    public FormatoInvalido(String msj){
        super(msj);
    }
}
