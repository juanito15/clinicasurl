/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoclinica;
import Clases.Conexion;
import Clases.Configuracion;
import Excepciones.NoSePuedeConectar;
import Excepciones.UsuarioNoExiste;
import java.sql.SQLException;
import javax.swing.JOptionPane;

import javax.swing.JOptionPane;

/**
 *
 * @author OsLo
 */
public class Login extends javax.swing.JFrame {
     private Conexion conexion;

    /**
     * Creates new form NewJFrame
     */
    public Login() {
        try {
            if(!Configuracion.SERVER_CONFIG_DEFAULT_FILE.exists()){
                GestorConexion gestor=new GestorConexion(this, true,false);
                while(!gestor.seCreoConfiguracion()){
                    JOptionPane.showMessageDialog(this, "No existe configuración para conectar a la base de datos, se abrirá una ventana para configurarla", "Configuración a la base de datos", JOptionPane.INFORMATION_MESSAGE);
                    gestor.setVisible(true);
                }
                conexion=gestor.getConexion();
            }else{
                Configuracion confServer=new Configuracion(Configuracion.SERVER_CONFIG_DEFAULT_FILE);
                conexion=new Conexion(confServer.getUser(),confServer.getIp(),confServer.getPass(),confServer.getBD());
            }          
            initComponents();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex, "Error", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        userField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        Ingresar = new javax.swing.JButton();
        passField = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Usuario:");

        jLabel2.setText("Contraseña:");

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        jLabel3.setText("Inicio de Sesión");

        Ingresar.setText("Ingresar");
        Ingresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IngresarActionPerformed(evt);
            }
        });

        passField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                passFieldKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Ingresar)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(userField, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                        .addComponent(passField)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(59, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(54, 54, 54))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(passField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(Ingresar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void IngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IngresarActionPerformed
       login();  
            
    }//GEN-LAST:event_IngresarActionPerformed

     private void gestorButtonActionPerformed(java.awt.event.ActionEvent evt) {                                             
        proyectoclinica.GestorConexion gestor= new proyectoclinica.GestorConexion(this,true,true);
        gestor.setVisible(true);
    } 
    private void passFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_passFieldKeyPressed
        //Si se presiona ENTER se trata de iniciar sesión
         if(evt.getKeyCode()==java.awt.event.KeyEvent.VK_ENTER)
            login();
    }//GEN-LAST:event_passFieldKeyPressed

     private void login(){
        try{
            //Si falta el usuario o la contraseña, se manda un mensaje pidiendo que se ingresen, de lo contrario se procede a intentar iniciar sesión
            if(userField.getText().trim().equals("")||new String(passField.getPassword()).trim().equals("")){
                JOptionPane.showMessageDialog(this, "Debe ingresar usuario y contraseña para iniciar sesión", "Advertencia", JOptionPane.WARNING_MESSAGE);
                passField.setText("");
                userField.requestFocus();
            }else{
                //Se llama al método en la BD que regresa true si los datos son correctos y false de lo contrario 
                if(conexion.login(userField.getText(),new String(passField.getPassword()))){
                    //Si los datos son correctos se muestra el menú principal
                    Principal menu= new Principal(conexion);
                    this.setVisible(false);
                    menu.setVisible(true);
                }
            }
        }catch(UsuarioNoExiste ex){
            //Si el usuario no existe en la BD se manda un mensaje indicandolo y se limpia el formulario
            JOptionPane.showMessageDialog(this, "Este usuario no existe en el sistema, compruebe los datos", "Advertencia", JOptionPane.WARNING_MESSAGE);
            userField.setText("");
            passField.setText("");
        } catch (NoSePuedeConectar ex) {
            JOptionPane.showMessageDialog(this, "Error de conexión a la base de datos, consulte al administrador del sistema"+System.getProperty("line.separator")+ex, "Advertencia", JOptionPane.WARNING_MESSAGE);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error de consuta en la base de datos, consulte al administrador del sistema"+System.getProperty("line.separator")+ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Ingresar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPasswordField passField;
    private javax.swing.JTextField userField;
    // End of variables declaration//GEN-END:variables
}
