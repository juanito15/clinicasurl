/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Excepciones.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Clase utilizada para guardar las configuraciones de una conexión a un servidor, a un archivo
 * @author juanm
 */
public class Configuracion {
    //Variable de donde se guarda el archivo por default
    //public static final File SERVER_CONFIG_DEFAULT_FILE=new File("server.conf");
    public static final File SERVER_CONFIG_DEFAULT_FILE=new File(System.getProperty("user.home")+File.separator+"SOFI"+File.separator+"server.conf");
    // Varibles de conexión a la BD, por defecto
    private String ip="localhost", pass="", user="",bd="";
    //Marca de inicio del archivo
    private final String marcaInicio="SOFIFile";
    //Array de bytes de la contraseña cifrada
    private byte[] passArray;
    //Clave de cifrado para el archivo (debe tener 16 caracteres)
    private final String claveCifrado="SOFI Tortillas PW";
    /**
     * Constructor vacío, para añadirle datos luego
     */
    public Configuracion(){}
    /**
     * Constructor para guardar en archivo los datos de conexión
     * @param ip ip del servidor
     * @param user usuario del SGBD
     * @param clave clave del usuario
     * @param bd nombre de la base de datos
     * @throws java.lang.Exception en caso de cualquier error
     */
    public Configuracion(String ip,String user, String clave, String bd) throws Exception{
        //Ciframos la clave que se especifica
        passArray=cifrarClave(clave, claveCifrado);
        this.pass=clave;
        this.ip=ip;
        this.user=user;
        this.bd=bd;
        
    }
    /**
     * Cifra la contraseña especificada, con AES de 16 bytes 
     * @param contenidoACifrar texto que desea cifrarse
     * @param claveDeCifrado contraseña con la cual va a cifrarse
     * @return arreglo de bytes que es el texto cifrado
     * @throws Exception en caso de errores
     */
    public byte[] cifrarClave(String contenidoACifrar, String claveDeCifrado) throws Exception{
        try{    
            // Generamos una clave que queramos que tenga al menos 16 bytes adecuada para AES
            Key key = new SecretKeySpec(claveDeCifrado.getBytes(),  0, 16, "AES");
            // Se obtiene un cifrador AES
            Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
            // Se inicializa el cifrador, se pone en modo de cifrado y se le envia la clave
            aes.init(Cipher.ENCRYPT_MODE,key);
            // Se encripta
            return aes.doFinal(contenidoACifrar.getBytes());
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException ex) {
            throw ex;
        }
    }
    /**
     * Cifra la contraseña especificada, con AES de 16 bytes 
     * @param contenidoADescifrar texto que desea cifrarse
     * @param claveDeCifrado contraseña con la cual va a cifrarse
     * @return arreglo de bytes que es el texto cifrado
     * @throws Exception en caso de errores
     */
    public String descifrarClave(byte[] contenidoADescifrar, String claveDeCifrado) throws Exception{
        try{    
            // Generamos una clave que queramos que tenga al menos 16 bytes adecuada para AES
            Key key = new SecretKeySpec(claveDeCifrado.getBytes(),  0, 16, "AES");
            // Se obtiene un cifrador AES
            Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
            // Se inicializa el cifrador, se pone en modo de descifrado y se le envia la clave
            aes.init(Cipher.DECRYPT_MODE,key);
            // Se desencripta y se guarda en la variable de servidor
            return new String(aes.doFinal(contenidoADescifrar));
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException ex) {
            throw ex;
        }
    }
    /**
     * Constructor que crea una estructura de la clase, extrayendo los datos del archivo de configuración
     * @param archivo
     * @throws FileNotFoundException si el archivo no se encuentra
     * @throws FormatoInvalido si el archivo no pertenece a este programa
     * @throws ArchivoNoExiste si el archivo no existe
     */
    public Configuracion(File archivo) throws FileNotFoundException, FormatoInvalido, ArchivoNoExiste, Exception{
        try {
            if(archivo.exists()){
                ip=user=pass=bd="";
                passArray=null;
                //Creamos un RandomAccessFile para leer el archivo de configuración
                RandomAccessFile file = new RandomAccessFile(archivo, "r");
                //Vamos leyendo el archivo
                int leer=file.read();
                //Leemos los primeros bytes del archivo para saber si la marca del archivo es correcta
                String marca="";
                if(file.length()>marcaInicio.length()){
                    for(int i=0;i<marcaInicio.length();i++){
                        marca+=(char)leer;
                        leer=file.read();
                    }
                }
                else
                    //Si no es correcta, lanzamos una excepción
                    throw new FormatoInvalido("El formato del archivo '"+archivo.getCanonicalPath()+"' no es válido");
                //Si la marca del archivo coincide, seguimos leyendo
                if(marca.equals(marcaInicio)){
                    //Leemos hasta encontrar el separador (byte 0)
                    //Leemos la IP del servidor donde esta el SGBD
                    while(leer!=0){
                        ip+=(char)leer;
                        leer=file.read();
                    }
                    leer=file.read();
                    //Leemos el nombre de la BD
                    while(leer!=0){
                        bd+=(char)leer;
                        leer=file.read();
                    }
                    leer=file.read();
                    //Leemos el usuario del SGBD
                    while(leer!=0){
                        user+=(char)leer;
                        leer=file.read();
                    }
                    leer=file.read();
                    //Leemos la contraseña, encriptada, al SGBD, hasta el final del archivo
                    ArrayList<Byte> contraTemp= new ArrayList<>();
                    while(leer!=-1){
                        contraTemp.add((byte)leer);
                        leer=file.read();
                    }
                    //Se inicializa el array de datos de contraseña
                    passArray=new byte[contraTemp.size()];
                    //Se guardan los datos en el array
                    for(int i=0;i<contraTemp.size();i++){
                        passArray[i]=contraTemp.get(i);
                    }
                    file.close();
                    pass=descifrarClave(passArray, claveCifrado);
                }else{
                    throw new FormatoInvalido("El formato del archivo '"+archivo.getCanonicalPath()+"' no es válido");
                }
            }else{
                throw new ArchivoNoExiste("El archivo de configuración no existe");
            }
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    /**
     * Escribe en un archivo las configuraciones
     * @param file archivo a escribir
     * @throws IOException en caso de que no pueda escribirse en el archivo
     */
    public void escribirArchivo(File file) throws IOException{
        try {
            //Comprobamos que la ruta donde se guardará el archivo exista, si no, se crea
            if(!file.getParentFile().exists())
                if(!file.getParentFile().mkdirs())
                    throw new IOException("No puede escribirse en el archivo de configuración, no se ha podido crear el directorio en donde va a guardarse");
            //Comprobamos si el archivo ya existe o si tiene datos
            if(!file.exists()||file.length()==0){
                //Creamos un RandomAccessFile para escribir el archivo
                RandomAccessFile archivo= new RandomAccessFile(file,"rw");
                //Escribimos la marca del archivo
                archivo.writeBytes(marcaInicio);
                //Escribimos la ip
                archivo.writeBytes(ip);
                //Escribimos el separador
                archivo.write(0);
                //Escribimos el nombre de la base de datos
                archivo.writeBytes(bd);
                //Escribimos  separador
                archivo.write(0);
                //Escribimos nombre de usuario
                archivo.writeBytes(user);
                //Separador
                archivo.write(0);
                //Escribimos la contraseña (cifrada) y cerramos el archivo
                archivo.write(passArray);
                archivo.close();
            }else{
                //Si el archivo ya existe y tiene contenido, lo vaciamos
                BufferedWriter bw = new BufferedWriter(new FileWriter(file));
                bw.write("");
                bw.close();
                escribirArchivo(file);    
            }
        } catch (IOException ex) {
            throw new IOException("No puede escribirse en el archivo de configuración, error"+System.getProperty("line.separator")+ex.getMessage());
        }
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
    public void setBd(String bd) {
        this.bd = bd;
    }
    public String getIp() {
        return ip;
    }
    public String getPass() {
        return pass;
    }
    public String getBD() {
        return bd;
    }
    public String getUser() {
        return user;
    }
}
