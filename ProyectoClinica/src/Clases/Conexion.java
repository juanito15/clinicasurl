/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.SQLException;
import Excepciones.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author juanm
 */
public class Conexion {
    public Connection conexion;//variable que servira para la conexión a la base de datos
    private final String driver="com.mysql.jdbc.Driver", url="jdbc:mysql://"; //variables que serivran en la conexion, estas nunca deben ser modificados
    private String user="root", ip="localhost", pass="", nombreBD=""; //Variables que pueden ser modificadas y por defecto son las que se muestran
    public final static int CONSULTA=0, ACTIVA=1, PAR_COMPROMETIDA=2,FALLIDA=3,ABORTADA=4,COMPROMETIDA=5, PUNTO_GUARDADO=6;
    private BufferedWriter escritorLog;
    private int noTransaccion = 1;
    private File bitacora;
    /**
     * Construye un objeto conexión con datos especificos del servidor con el SGBD
     * @param user usuario de acceso
     * @param ip ip del servidor
     * @param pass contraseña del usuario
     * @param db base de datos a conectarse
     * @throws java.io.IOException en caso de no poder escribir la bitácora
     */
    public Conexion(String user, String ip, String pass, String db) throws IOException
    {
        //Creamos el objeto que escribirá la bitácora
        String folder=System.getProperty("user.home")+File.separator+".SOFI";
        bitacora= new File(folder);
        //Obtenemos la fecha de la computadora
        java.util.GregorianCalendar calendario=new java.util.GregorianCalendar();
        java.util.Date fechaActual=calendario.getTime();
        //Comprobamos que la carpeta '.SOFI' exista, si no la creamos
        if(!bitacora.exists()){
            if(!bitacora.mkdir())
                throw new IOException("No se pudo crear el directorio");
        }
        //Comprobamos que la carpeta '.SOFI/<Año>' exista, si no la creamos
        folder+=File.separator+(1900+fechaActual.getYear());
        bitacora=new File(folder);
        if(!bitacora.exists()){
            if(!bitacora.mkdir())
                throw new IOException("No se pudo crear el directorio");
        }
        //Comprobamos que la carpeta '.SOFI/<Año>/<Mes>' exista, si no la creamos
        folder+=File.separator+(1+fechaActual.getMonth());
        bitacora=new File(folder);
        if(!bitacora.exists()){
            if(!bitacora.mkdir())
                throw new IOException("No se pudo crear el directorio");
        }
        //Creamos el archivo con la fecha de hoy
        folder+=File.separator+fechaActual.getDate()+".log";
        bitacora=new File(folder);
        escritorLog=new BufferedWriter(new java.io.FileWriter(bitacora, true));
        //Asignamos los parámetros de la conexión a la BD
        this.user=user;
        this.ip=ip;
        this.pass=pass;
        this.nombreBD=db;
    }
    /**
     * Metodo que genera la conexion utilizando los atributos de esta clase
     * @param nombreBD el nombre de la base de datos a conectarse
     */
    public void conectar() throws NoSePuedeConectar
    {
       conexion=null;
        try{
            Class.forName(driver);//Se utiliza el driver de conexion
            conexion=DriverManager.getConnection(url+ip+"/"+nombreBD,user,pass);//Se conecta con la base de datos
        }catch (SQLException|NullPointerException ex) {
            throw new NoSePuedeConectar("No puede conectarse a la BD, error:"+System.getProperty("line.separator")+ex.toString());
        } catch (ClassNotFoundException ex) {
            throw new NoSePuedeConectar("Error al registrar el driver de MySQL, error:"+System.getProperty("line.separator")+ex.toString());
        }
    }
    /**
    * Metodo que genera la conexion para saber si puede conectarse a la BD
    * @return true si se puede conectar a la base con los datos ingresados, false de lo contrario
    */
    public boolean probarConexion ()
    {
       conexion=null;
        try{
            Class.forName(driver);//Se utiliza el driver de conexion
            conexion=DriverManager.getConnection(url+ip+"/"+nombreBD,user,pass);//Se conecta con la base de datos
            conexion.close();
            return true;
        }catch (SQLException|ClassNotFoundException ex) {
            return false;
        }
    }
    /**
     * Funció que inicia sesión, a traves de una función almacenada en la BD
     * @param usuario usuario a iniciar sesión
     * @param clave contraseña del usuario
     * @return True si los datos son correctos, false de lo contrario
     * @throws UsuarioNoExiste en caso de que el usuario no exista
     * @throws NoSePuedeConectar en caso de no poder conectar a la BD
     * @throws SQLException en caso de error SQL
     */
    public boolean login(String usuario, String clave) throws UsuarioNoExiste,NoSePuedeConectar, SQLException{
        int resultado=0;
        conectar();
        //Creamos instruccion para la BD
        Statement instruccion = conexion.createStatement();
        //Ejecutamos la instrucción en la BD
        ResultSet filas=instruccion.executeQuery("SELECT login('"+usuario+"','"+clave+"');");
        while(filas.next())
            //Guardamos el resultado, el cual es 1 si los datos son correctos, -1 si el usuario no existe y 0 si la contraseña es incorrecta
            resultado=filas.getInt(1);
        //Lanzamos una excepción en caso de que el usuario no exista
        if(resultado==-1)
            throw new UsuarioNoExiste("El usuario especificado no existe en el sistema");
        return resultado==1;
    }
    /**
     * Ejecuta una sentencia en la BD (creado para uso de INSERT, REMOVE, UPDATE o REPLACE)
     * @param query sentencia a ejecutar en la BD
     * @param estadoTransaccion estado de la transacción para la bitácora
     * @return filas afectadas en la base de datos
     * @throws SQLException en caso de error de SQL
     * @throws NoSePuedeConectar en caso de no poder conectar a la BD
     * @throws java.io.IOException en caso de no poder escribir en la bitácora
     * @see Conexion.wLog
     */
    public int consultaModificacion(String query, int estadoTransaccion) throws SQLException, NoSePuedeConectar, IOException{
        //Se conecta a la BD
        conectar();
        //Se crea una instrucción
        Statement instruccion=conexion.createStatement();
        //Se escribe en la bitácora la sentencia
        wLog(estadoTransaccion, query);
        //Se hace la instrucción en la BD y se devuelve el número de filas afectadas
        int filas=instruccion.executeUpdate(query);
        //Cerramos la conexión y retornamos las filas afectadas
        conexion.close();
        return filas;
    }
    
    /**
     * Ejecuta una sentencia en la BD (creado para uso de INSERT, REMOVE, UPDATE o REPLACE)
     * @param query sentencia a ejecutar en la BD
     * @param estadoTransaccion estado de la transacción para la bitácora
     * @return filas afectadas en la base de datos
     * @throws SQLException en caso de error de SQL
     * @throws NoSePuedeConectar en caso de no poder conectar a la BD
     * @throws java.io.IOException en caso de no poder escribir en la bitácora
     * @see Conexion.wLog
     */
    public int consultaInsertReturnId(String query, int estadoTransaccion) throws SQLException, NoSePuedeConectar, IOException{
        //Se conecta a la BD
        conectar();
        //Se crea una instrucción
        Statement instruccion=conexion.createStatement();
        //Se escribe en la bitácora la sentencia
        wLog(estadoTransaccion, query);
        //Se hace la instrucción en la BD y se devuelve el id del valor insertado en la tabla
        instruccion.executeUpdate(query);
        
        ResultSet filas = instruccion.executeQuery("SELECT LAST_INSERT_ID();");
        int id = 0;
        if (filas.next()) {
            id = filas.getInt(1);
        }
        //Cerramos la conexión y retornamos las filas afectadas
        conexion.close();
        return id;
    }
    
    /**
     * Ejecuta una sentencia en la BD (creado para uso de INSERT, REMOVE, UPDATE o REPLACE)
     * @param query sentencia a ejecutar en la BD
     * @param estadoTransaccion estado de la transacción para la bitácora
     * @return resultado de la consulta
     * @throws SQLException en caso de error de SQL
     * @throws NoSePuedeConectar en caso de no poder conectar a la BD
     * @see Conexion.wLog
     */
    public ResultSet consultaDatos(String query, int estadoTransaccion) throws SQLException, NoSePuedeConectar{
        //Se conecta a la BD
        //conectar();
        //Se crea una instrucción
        Statement instruccion=conexion.createStatement();
        //Se hace la instrucción en la BD y se devuelve el número de filas afectadas
        ResultSet filas=instruccion.executeQuery(query);
        //Cerramos la conexión y retornamos las filas afectadas
       // conexion.close();
        return filas;
    }
    /**
     * Inicia una transacción, mandando un 'BEGIN' a la BD
     * @throws SQLException en caso de error SQL
     * @throws NoSePuedeConectar en caso de no poder conectar a la BD
     */
    public void inicioTransaccion() throws SQLException, NoSePuedeConectar{
        //Conecta a la BD
        conectar();
        //Crea una instrucción
        Statement instruccion=conexion.createStatement();
        //Ejecuta el BEGIN
        instruccion.execute("BEGIN");
        //Cierra la conexión
        conexion.close();
    }
    /**
     * Finaliza una transacción
     * @param confirmar true si se confirma la transacción ('COMMIT') o se aborta ('ROLLBACK')
     * @throws NoSePuedeConectar en caso de no poder conectar a la BD
     * @throws SQLException en caso de error SQL
     */
    public void finTransaccion(boolean confirmar) throws NoSePuedeConectar, SQLException{
        //Conecta a la BD
        conectar();
        //Crea una instrucción
        Statement instruccion=conexion.createStatement();
        //Dependiendo del parámetro, se confirma o aborta la transacción
        if(confirmar)
            instruccion.execute("COMMIT");
        else
            instruccion.execute("ROLLBACK");
        //Cierra la conexión a la BD
        conexion.close();
    }
    /**
     * Escribe en el log del programa, dependiendo de los parámetros que se le envien, los datos del estado
     * de la transacción e información adicional que se envia en la variable 'texto'.
     * Las claves de cada estado, son las siguientes:
     *  <table summary="Muestra los Códigos de 'estadoTransaccion'">
     * <tr><th>Código</th> <th>Variable <b>estática</b> que lo representa</th> <th>Significado</th></tr>
     * <tr><td><code>0</code></td>
     *     <td>CONSULTA</td>
     *     <td>Indica que es una consulta perteneciente a la transacción</td></tr>
     * <tr><td><code>1</code></td>
     *     <td>ACTIVA</td>
     *     <td>Indica que se ejecutó un 'BEGIN;' en la BD (inicio de una transacción)</td></tr>
     * <tr><td><code>2</code></td>
     *     <td>PAR_COMPROMETIDA</td>
     *     <td>Indica que la transacción está parcialmente comprometida (se ha ejecutado su ultima consulta)</td></tr>
     * <tr><td><code>3</code></td>
     *     <td>FALLIDA</td>
     *     <td>Indica que la transacción está fallida (no pudo completarse <b>por algún error</b></td></tr>
     * <tr><td><code>4</code></td>
     *     <td>ABORTADA</td>
     *     <td>Se deshicieron los cambios en la BD hechos por la transacción (envío de 'ROLLBACK;' a la BD)</td></tr>
     * <tr><td><code>5</code></td>
     *     <td>COMPROMETIDA</td>
     *     <td>Indica que la transacción se finalizó correctamente, guardando todos los cambios hechos por la misma (envío de 'COMMIT;' a la BD)</td></tr>
     * <tr><td><code>6</code></td>
     *     <td>PUNTO_GUARDADO</td>
     *     <td>Indica que se pasó por un punto de guardado ('SAVE POINT') para evitar pérdida de información</td></tr>
     * </table>
     * @param estadoTransaccion Código de estado de transacción
     * @param texto texto a escribir con el estado de la transaccion enviada
     */
    public void wLog(int estadoTransaccion, String texto) throws IOException{
        if(texto==null)
            texto="";
        //Obtenemos la fecha actual
        java.util.GregorianCalendar calendario=new java.util.GregorianCalendar();
        java.util.Date fechaTranc=calendario.getTime();
        String  fecha="<"+(1900+fechaTranc.getYear())+"/"+(fechaTranc.getMonth()+1)+"/"+fechaTranc.getDate()+" "+fechaTranc.getHours()+":"+fechaTranc.getMinutes()+":"+fechaTranc.getSeconds()+">";
        
        //Comprobamos que haya un mensaje en la bitácora
        if(bitacora.length()>0)
            escritorLog.newLine();
        //Creamos la cadena a escribir
        String escribir="";
        //Dependiendo del estado de la transacción se escriben diferentes tipos de cosas
        switch(estadoTransaccion){
            case 0:
                escribir=fecha+" "+texto;
                break;
            case 1:
                escribir=fecha+" TRANSACCIÓN "+noTransaccion+" ACTIVA"+(texto.trim().equals("")?"":": "+texto);
                break;
            case 2:
                escribir=fecha+" TRANSACCIÓN "+noTransaccion+" PARCIALMENTE COMPROMETIDA"+(texto.trim().equals("")?"":": "+texto);
                break;
            case 3:
                escribir=fecha+" TRANSACCIÓN "+noTransaccion+" FALLIDA"+(texto.trim().equals("")?"":": "+texto);
                break;
            case 4:
                escribir=fecha+" TRANSACCIÓN "+noTransaccion+" ABORTADA"+(texto.trim().equals("")?"":": "+texto);
                noTransaccion++;
                break;
            case 5:
                escribir=fecha+" TRANSACCIÓN "+noTransaccion+" COMPROMETIDA";
                noTransaccion++;
                break;
            case 6:
                escribir=fecha+" PUNTO DE SALVADO"+(texto.trim().equals("")?"":" "+texto);
            default:
                break;
        }
        //Escribe la nueva línea al final del archivo
        escritorLog.append(escribir);
    }

}
