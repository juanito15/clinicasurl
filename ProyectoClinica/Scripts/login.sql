﻿DELIMITER //
DROP FUNCTION IF EXISTS login//
CREATE FUNCTION login (vNombre VARCHAR(45), vPass VARCHAR(45)) RETURNS INT
BEGIN
	DECLARE comparar INT UNSIGNED DEFAULT 0;
	DECLARE contraCif BLOB;
	#Verifica que el usuario no exista
	SELECT COUNT(*) FROM usuarios WHERE nombre=vNombre INTO comparar;
	IF (comparar!=0) THEN
		#Obtiene la contraseña cifrada
		SELECT clave FROM usuarios WHERE nombre=vNombre INTO contraCif;
		#Compara si la contraseña introducida es igual a la encriptada de la BD, se devuelve 1 si son iguales, 0 de lo contrario
		RETURN MD5(vPass)=contraCif;
	ELSE
		#Se devuelve -1 si el usuario no existe en la BD
		RETURN -1;
	END IF;
END//
DELIMITER ;