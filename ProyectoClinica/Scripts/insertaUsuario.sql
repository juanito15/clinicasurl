﻿DELIMITER //
DROP FUNCTION IF EXISTS creacionUsuarioNuevo//
CREATE FUNCTION creacionUsuarioNuevo (vNombreUsuario VARCHAR(45), vContraUsuario VARCHAR(45)) RETURNS INT
BEGIN
	DECLARE comparar INT UNSIGNED DEFAULT 0;
	DECLARE IDUltimoUsuario INT UNSIGNED DEFAULT 0;
	#Verifica que el usuario no exista
	SELECT COUNT(*) FROM usuarios WHERE nombre=vNombreUsuario INTO comparar;
	#Si el usuario ya existe, se devuelve 0, de lo contrario, lo inserta y devuelve 1
	IF (comparar=0) THEN
		#Inserta el nuevo usuario en la BD, encriptando la contraseña con MD5
		INSERT INTO usuarios (nombre,clave) VALUES (vNombreUsuario, MD5(vContraUsuario));
		RETURN 1;
	ELSE
		RETURN 0;
	END IF;
END//
DELIMITER ;